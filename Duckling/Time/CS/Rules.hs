-- Copyright (c) 2016-present, Facebook, Inc.
-- All rights reserved.
--
-- This source code is licensed under the BSD-style license found in the
-- LICENSE file in the root directory of this source tree.
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoRebindableSyntax #-}

module Duckling.Time.CS.Rules
  ( rules,
  )
where

import Data.Maybe
import Duckling.Dimensions.Types
import Duckling.Numeral.Helpers (parseInt)
import Duckling.Regex.Types
import Duckling.Time.Computed (easterSunday)
import Duckling.Time.Helpers
import Duckling.Time.Types (TimeData (..))
import Duckling.Time.Types (TimeData (..), TimeIntervalType (..))
import qualified Duckling.TimeGrain.Types as TG
import Duckling.Types
import Prelude
import qualified Data.Text as Text

ruleDDMM :: Rule
ruleDDMM =
  Rule
    { name = "dd/mm",
      pattern =
        [ regex "(3[01]|[12]\\d|0?[1-9])\\s?[/-]\\s?(1[0-2]|0?[1-9])"
        ],
      prod = \tokens -> case tokens of
        (Token RegexMatch (GroupMatch (dd : mm : _)) : _) -> do
          d <- parseInt dd
          m <- parseInt mm
          tt $ monthDay m d
        _ -> Nothing
    }

ruleDDMMRRRR :: Rule
ruleDDMMRRRR =
  Rule
    { name = "dd/mm/rrrr",
      pattern =
        [ regex "(3[01]|[12]\\d|0?[1-9])[-/\\s](1[0-2]|0?[1-9])[-/\\s](\\d{2,4})"
        ],
      prod = \tokens -> case tokens of
        (Token RegexMatch (GroupMatch (dd : mm : rr : _)) : _) -> do
          r <- parseInt rr
          d <- parseInt dd
          m <- parseInt mm
          tt $ yearMonthDay r m d
        _ -> Nothing
    }

ruleDDMMRRRRDot :: Rule
ruleDDMMRRRRDot =
  Rule
    { name = "dd.mm.rrrr",
      pattern =
        [ regex "(3[01]|[12]\\d|0?[1-9])\\.(1[0-2]|0?[1-9])\\.(\\d{4})"
        ],
      prod = \tokens -> case tokens of
        (Token RegexMatch (GroupMatch (dd : mm : rr : _)) : _) -> do
          r <- parseInt rr
          d <- parseInt dd
          m <- parseInt mm
          tt $ yearMonthDay r m d
        _ -> Nothing
    }

ruleInstants :: [Rule]
ruleInstants = mkRuleInstants
  [ ("right now"    , TG.Second, 0  , "((právě|hned)\\s*)teď|nyní|teďka")
  , ("today"        , TG.Day   , 0  , "dneska|dnes|(v tento den)"           )
  , ("tomorrow"     , TG.Day   , 1  , "(zítra|zejtra)"            )
  , ("day after tomorrow"     , TG.Day   , 2  , "(pozítří|pozejtří)"            )
  , ("2 days after tomorrow"     , TG.Day   , 3  , "(popozítří|popozejtří)"            )
  , ("yesterday"    , TG.Day   , - 1, "včera"                      )
  , ("day before yesterday"     , TG.Day   , -2  , "(předevčírem|předvčírem)"            )
  ]

ruleNow :: Rule
ruleNow = Rule
  { name = "now"
  , pattern =
    [ regex "teď|v tento okamžik|teďka|nyní"
    ]
  , prod = \_ -> tt now
  }

ruleDaysOfWeek :: [Rule]
ruleDaysOfWeek = mkRuleDaysOfWeek
  [ ( "Pondělí"   , "pondělí"         )
  , ( "Úterý"  , "úterý"      )
  , ( "Středa", "střed(a|u|y|ě|ou)"     )
  , ( "Čtvrtek" , "čtvrt(ek|ka|ku|kem)" )
  , ( "Pátek"   , "pát(ek|ku|kem)"         )
  , ( "Sobota" , "sobot(a|y|ě|u|ou)"       )
  , ( "Neděle"   , "neděl(e|i|í)"         )
  ]

ruleMonths :: [Rule]
ruleMonths = mkRuleMonthsWithLatent
  [ ( "Leden"  , "led(en|na|nu|nem)"    , False )
  , ( "Únor" , "únor(a|u|em)?"   , False )
  , ( "Březen"    , "břez(en|na|nu|nem)"      , False )
  , ( "Duben"    , "dub(en|na|nu|nem)"      , False )
  , ( "Květen"      , "květ(en|na|nu|nem)"          , False  )
  , ( "Červen"     , "červ(en|na|nu|nem)"       , False )
  , ( "Červenec"     , "červen(ec|ce|ci|cem)"       , False )
  , ( "Srpen"   , "srp(en|na|nu|nem)"     , False )
  , ( "Září", "září", False )
  , ( "Říjen"  , "říj(en|na|nu|nem)"    , False )
  , ( "Listopad" , "listopad(u|em)?"   , False )
  , ( "Prosinec" , "prosin(ec|ce|ci|cem)"   , False )
  ]

ruleAbsorbOnDay :: Rule
ruleAbsorbOnDay = Rule
  { name = "on <day>"
  , pattern =
    [ regex "(ve|v)"
    , Predicate $ isGrainOfTime TG.Day
    ]
  , prod = \tokens -> case tokens of
      (_:token:_) -> Just token
      _ -> Nothing
  }

ruleNextDOW :: Rule
ruleNextDOW = Rule
  { name = "this|next <day-of-week>"
  , pattern =
    [ regex "(toto|tuto|tento|příští)"
    , Predicate isADayOfWeek
    ]
  , prod = \case
      (
        Token RegexMatch (GroupMatch (match:_)):
        Token Time dow:
        _) -> do
          td <- case Text.toLower match of
                  "tento" -> Just $ predNth 0 True dow
                  "příští" -> intersect dow $ cycleNth TG.Week 1
                  _ -> Nothing
          tt td
      _ -> Nothing
  }

ruleThisTime :: Rule
ruleThisTime = Rule
  { name = "this <time>"
  , pattern =
    [ regex "toto|tuto|tento|aktuální|probíhající|o"
    , Predicate isOkWithThisNext
    ]
  , prod = \tokens -> case tokens of
      (_:Token Time td:_) -> tt $ predNth 0 False td
      _ -> Nothing
  }

ruleNextTime :: Rule
ruleNextTime = Rule
  { name = "next <time>"
  , pattern =
    [ regex "příští"
    , Predicate isOkWithThisNext
    ]
  , prod = \tokens -> case tokens of
      (_:Token Time td:_) -> tt $ predNth 0 True td
      _ -> Nothing
  }

ruleLastTime :: Rule
ruleLastTime = Rule
  { name = "last <time>"
  , pattern =
    [ regex "(uplynulý|uplynulé|uplynulou|minulý|minulé|minulou|předešlý|předešlé|předešlou|předchozí)"
    , Predicate isOkWithThisNext
    ]
  , prod = \tokens -> case tokens of
      (_:Token Time td:_) -> tt $ predNth (- 1) False td
      _ -> Nothing
  }

ruleWeekend :: Rule
ruleWeekend = Rule
  { name = "week-end"
  , pattern =
    [ regex "(víkendu|víkendem|víkend)"
    ]
  , prod = \_ -> tt $ mkOkForThisNext weekend
  }

ruleWeek :: Rule
ruleWeek = Rule
 { name = "week"
 , pattern =
   [ regex "(celý|zbytek|po zbytek|tento) (týden|týdne)"
   ]
 , prod = \case
     (Token RegexMatch (GroupMatch (match:_)):_) ->
       let end = cycleNthAfter True TG.Day (-2) $ cycleNth TG.Week 1
           period = case Text.toLower match of
                      "celý" -> interval Closed (cycleNth TG.Week 0) end
                      "zbytek" -> interval Open today end
                      "tento" -> interval Open today end
                      _ -> Nothing
       in case Text.toLower match of
         "tento" -> Token Time . mkLatent <$> period
         _ -> Token Time <$> period
     _ -> Nothing
 }

ruleCycleThisLastNext :: Rule
ruleCycleThisLastNext = Rule
  { name = "this|last|next <cycle>"
  , pattern =
    [ regex "(tento|současný|nynější|minulý|uplynulý|předešlý|před|příští|za|následující|nadcházející)"
    , dimension TimeGrain
    ]
  , prod = \case
      (
        Token RegexMatch (GroupMatch (match:_)):
        Token TimeGrain grain:
        _) ->
          case Text.toLower match of
            "tento"          -> tt $ cycleNth grain 0
            "současný"        -> tt $ cycleNth grain 1
            "nynější"       -> tt $ cycleNth grain 0
            "minulý"          -> tt $ cycleNth grain $ - 1
            "uplynulý"          -> tt $ cycleNth grain $ - 1
            "předešlý"      -> tt $ cycleNth grain $ - 1
            "před"      -> tt $ cycleNth grain $ - 1
            "příští"          -> tt $ cycleNth grain 1
            "za"          -> tt $ cycleNth grain 1
            "následující"      -> tt $ cycleNth grain 1
            "nadcházející" -> tt $ cycleNth grain 1
            _ -> Nothing
      _ -> Nothing
  }

ruleDayInDuration :: Rule
ruleDayInDuration = Rule
  { name = "<day> in <duration>"
  , pattern =
    [ Predicate $ or . sequence [isGrainOfTime TG.Day, isGrainOfTime TG.Month]
    , regex "za"
    , Predicate $ isDurationGreaterThan TG.Hour
    ]
  , prod = \tokens -> case tokens of
      (Token Time td:_:Token Duration dd:_) ->
        Token Time <$> intersect td (inDurationInterval dd)
      _ -> Nothing
  }

ruleInDurationAtTime :: Rule
ruleInDurationAtTime = Rule
  { name = "in <duration> at <time-of-day>"
  , pattern =
    [ regex "za"
    , Predicate $ isDurationGreaterThan TG.Hour
    , regex "v"
    , Predicate isATimeOfDay
    ]
  , prod = \tokens -> case tokens of
      (_:Token Duration dd:_:Token Time td:_) ->
        Token Time <$> intersect td (inDurationInterval dd)
      _ -> Nothing
  }

ruleInDuration :: Rule
ruleInDuration = Rule
  { name = "in <duration>"
  , pattern =
    [ regex "za"
    , dimension Duration
    ]
  , prod = \tokens -> case tokens of
      (_:Token Duration dd:_) ->
        tt $ inDuration dd
      _ -> Nothing
  }

rules :: [Rule]
rules =
  [ ruleDDMM
  , ruleDDMMRRRR
  , ruleDDMMRRRRDot
  , ruleAbsorbOnDay
  , ruleNextDOW
  , ruleNextTime
  , ruleThisTime
  , ruleLastTime
  , ruleWeekend
  , ruleWeek
  , ruleNow
  , ruleCycleThisLastNext
  , ruleDayInDuration
  , ruleInDurationAtTime
  , ruleInDuration
  ]
  ++ ruleInstants
  ++ ruleDaysOfWeek
  ++ ruleMonths
