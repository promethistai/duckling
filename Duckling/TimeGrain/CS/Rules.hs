-- Copyright (c) 2016-present, Facebook, Inc.
-- All rights reserved.
--
-- This source code is licensed under the BSD-style license found in the
-- LICENSE file in the root directory of this source tree.


{-# LANGUAGE OverloadedStrings #-}

module Duckling.TimeGrain.CS.Rules
  ( rules
  ) where

import Data.String
import Data.Text (Text)
import Prelude

import Duckling.Dimensions.Types
import Duckling.Types
import qualified Duckling.TimeGrain.Types as TG

grains :: [(Text, String, TG.Grain)]
grains = [ ("second (grain)" , "sekund(a|y|u|ě|ou|ách|ami)?",      TG.Second)
         , ("second (grain)" , "vetřin(a|y|u|ě|ou|ách|ami)?",      TG.Second)
         , ("minute (grain)" , "minut(a|y|u|ě|ou|ách|ami)?",       TG.Minute)
         , ("hour (grain)"   , "hodin(a|y|u|ě|ou|ách|ami)?",       TG.Hour)
         , ("day (grain)"    , "(dn(ech|em|e|i|u|y|í|ům|ů)|den)",  TG.Day)
         , ("week (grain)"   , "(týdn(ech|em|e|u|y|í|ům|ů)|týden)",TG.Week)
         , ("month (grain)"  , "měsíc(em|e|ích|i|ům|ů)?",          TG.Month)
         , ("quarter (grain)", "čtvrtletí(mi|m|ch)?",              TG.Quarter)
         , ("year (grain)"   , "ro(ku|ka|ky|ků|cích|ce)",          TG.Year)
         , ("year (grain)"   , "let(ům|ech|y)?",                   TG.Year)
         ]

rules :: [Rule]
rules = map go grains
  where
    go (name, regexPattern, grain) = Rule
      { name = name
      , pattern = [regex regexPattern]
      , prod = \_ -> Just $ Token TimeGrain grain
      }
